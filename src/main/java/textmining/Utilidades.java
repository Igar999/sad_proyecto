package textmining;

import weka.attributeSelection.InfoGainAttributeEval;
import weka.attributeSelection.Ranker;
import weka.classifiers.Classifier;
import weka.core.Attribute;
import weka.core.Instances;
import weka.core.SerializationHelper;
import weka.core.Utils;
import weka.core.converters.ArffSaver;
import weka.core.converters.ConverterUtils.DataSource;
import weka.filters.Filter;
import weka.filters.supervised.attribute.AttributeSelection;
import weka.filters.unsupervised.attribute.FixedDictionaryStringToWordVector;
import weka.filters.unsupervised.instance.NonSparseToSparse;
import weka.filters.unsupervised.instance.RemovePercentage;
import weka.filters.unsupervised.instance.SparseToNonSparse;

import java.io.*;
import java.util.*;

/**
 * Funcionalidades varias.
 *
 * @author Ander Cejudo
 * @author Igor Garcia
 * @author Aitor Javier Perez
 */
public class Utilidades {

    /**
     * Guarda un conjunto de instancias en un archivo con extensión .arff en la ruta especificada.
     *
     * @param instancias Conjunto de instancias a guardar.
     * @param ruta       Ruta con el nombre del archivo a crear.
     * @author Ander Cejudo
     */
    public static void guardarArffAbsoluto(Instances instancias, String ruta) {
        try {
            ArffSaver archivo = new ArffSaver();
            archivo.setInstances(instancias);
            archivo.setFile(new File(ruta));
            archivo.writeBatch();
        } catch (IOException e) {
            System.out.println("No se ha podido guardar en " + ruta);
        }
    }


    /**
     * Guarda un conjunto de instancias en un archivo con extensión .arff en la ruta en la que se ejecuta el programa.
     *
     * @param instancias Conjunto de instancias a guardar.
     * @param nombre     El nombre del archivo a crear.
     * @author Ander Cejudo
     */
    public static void guardarArffRelativo(Instances instancias, String nombre) {
        String pwd = System.getProperty("user.dir");
        try {
            ArffSaver archivo = new ArffSaver();
            archivo.setInstances(instancias);
            archivo.setFile(new File(pwd + "/" + nombre));
            archivo.writeBatch();
        } catch (IOException e) {
            System.out.println("No se ha podido guardar el archivo " + nombre + " en la ruta " + pwd);
        }
    }


    /**
     * Escribe un texto al path seleccionado.
     *
     * @param texto Es el texto a escribir y ruta es el directorio de destino.
     * @param ruta  Ruta en la que se va a guardar el fichero
     *              en el directorio ruta, salta una excepción.
     * @author Aitor Javier Perez
     */
    public static void escribirFicheroAbsoluto(String texto, String ruta) {
        FileWriter file = null;
        try {
            file = new FileWriter(ruta);
        } catch (IOException e) {
            System.out.println("Error al escribir el fichero...");
        }
        try {
            assert file != null;
            file.write(texto);
        } catch (IOException e) {
            System.out.println("Error al escribir el fichero...");
        }
        try {
            file.close();
        } catch (IOException e) {
            System.out.println("Error al escribir el fichero...");
        }
    }

    /**
     * Escribe el texto seleccionado en la ruta en la que se ejecuta el programa.
     *
     * @param texto  Texto a escribir.
     * @param nombre El nombre del archivo a crear.
     * @author Ander Cejudo
     */
    public static void escribirFicheroRelativo(String texto, String nombre) {
        String pwd = System.getProperty("user.dir");
        try {
            BufferedWriter bf = new BufferedWriter(new FileWriter(new File(pwd + "/" + nombre)));
            bf.write(texto);
            bf.flush();
            bf.close();
        } catch (IOException e) {
            System.out.println("No se ha podido escribir en " + pwd);
        }
    }

    /**
     * Dado un String con la ruta de un fichero, devuelve el conjunto de instancias.
     *
     * @param ruta La ruta del archivo .arff.
     * @return El conjunto de instancias asociado al fichero de la ruta.
     * @author Igor García
     */
    public static Instances archivo2Instancias(String ruta) {
        Instances instancias = null;
        try {
            //COMPRUEBA SI LOS ARCHIVOS DE ENTRADA USAN PATHS ABSOLUTOS O RELATIVOS
            String pwd = System.getProperty("user.dir");
            File f = new File(pwd + "/" + ruta);
            if (f.exists()) {
                //RELATIVO
                DataSource ds = new DataSource(pwd + "/" + ruta);
                instancias = ds.getDataSet();
            } else {
                //ABSOLUTO
                f = new File(ruta);
                if (f.exists()) {
                    DataSource ds = new DataSource(ruta);
                    instancias = ds.getDataSet();
                } else {
                    throw new Exception();
                }
            }
        } catch (Exception e) {
            System.out.println("No se ha podido importar el archivo desde " + ruta);
        }
        return instancias;
    }

    /**
     * Analiza el conjunto de instancias en busca de algún atributo de tipo String.
     *
     * @param instancias conjunto de instancias a utilizar.
     * @return True si lo ha encontrado.
     * @author Ander Cejudo
     */
    public static boolean tieneAtributoString(Instances instancias) {
        boolean tiene = false;
        try {
            if (!instancias.isEmpty()) {
                for (int i = 0; i < instancias.numAttributes(); i++) {
                    if (instancias.attribute(i).isString()) {
                        tiene = true;
                        break;
                    }
                }
            }
            return tiene;
        } catch (Exception e) {
            System.out.println("Fallo en saber si tiene atributo string...");
        }
        return tiene;
    }

    /**
     * Exporta el conjunto de datos a la ruta especificada.
     *
     * @param instancias Las instancias que se quieren guardar.
     * @param ruta       La ruta en la que se desea que esté el archivo resultante.
     * @author Igor García
     */
    public static void exportar(Instances instancias, String ruta) {
        try {
            //GUARDAR LAS INSTANCIAS  DE TEST BAG OF WORDS EN UN ARCHIVO
            ArffSaver saver = new ArffSaver();
            saver.setInstances(instancias);
            //COMPRUEBA SI LA RUTA DE SALIDA USA PATH ABSOLUTO O RELATIVO
            String pwd = System.getProperty("user.dir");
            File f = new File(pwd + ruta);
            if (f.exists()) {
                //PATH RELATIVO
                saver.setFile(new File(pwd + "/" + ruta));
                saver.writeBatch();
            } else {
                //PATH ABSOLUTO
                saver.setFile(new File(ruta));
                saver.writeBatch();
            }
        } catch (IOException e) {
            System.out.println("No se ha podido exportar el fichero en la ruta" + ruta);
        }
    }

    /**
     * A partir de un objeto Instances, crea un string que representa el diccionario de atributos, colocando el nombre de cada uno de los atributos de las instancias en una lína distinta.
     *
     * @param instancias Las instancias de las que se quiere obtener el diccionario.
     * @return Un String con todas las palabras del diccionario (las cuales correponden al conjunto de atributos).
     * @author Igor García
     */
    public static String diccionario(Instances instancias) {
        //ENUMERAR LOS ATRIBUTOS DEL TRAIN BAG OF WORDS
        Enumeration<Attribute> enA = instancias.enumerateAttributes();
        //TRANSFOTMAR LA ENUMERACIÓN EN UN ARRAY
        ArrayList<Attribute> aA = Collections.list(enA);
        //CREAR EL STRING DICCIONARIO (LA PRIMERA LÍNEA DEBE ESTAR VACÍA PARA QUE FUNCIONE BIEN, PORQUE EL FILTRO NO LEE LA PRIMERA LÍNEA)
        String dict = "\r\n";
        //PARA CADA ATRIBUTO DEL ARRAY, SE AÑADE AL STRING DICCIONARIO EN UNA LÍNEA NUEVA
        for (Attribute a : aA) {
            dict = dict + a.name() + "\r\n";
        }
        return dict;
    }

    /**
     * Transforma un conjunto de instancias de no diperso a disperso.
     *
     * @param testTransform El conjunto que se quiere transformar.
     * @return El mismo conjunto de entrada, pero no disperso.
     * @author Igor García
     */
    public static Instances sparseToNonSparse(Instances testTransform) {
        try {
            SparseToNonSparse filter = new SparseToNonSparse();
            filter.setInputFormat(testTransform);
            testTransform = Filter.useFilter(testTransform, filter);
        } catch (Exception e) {
            System.out.println("No se ha podido aplicar el filtro SparseToNonSparse");
        }
        return testTransform;
    }

    /**
     * Establece la clase del conjunto de instancias dados, en el caso de que el indice sea uno, se tomará como el último atributo
     * del conjunto.
     *
     * @param i          El índice a asignar.
     * @param instancias Conjunto al que se le va a asignar la clase.
     * @author Ander Cejudo
     */
    public static void setIndiceClase(int i, Instances instancias) {
        //EL -1 SE UTILIZA PARA ASIGNÁRSELO AL ÚLTIMO ATRIBUTO
        try {
            if (i == -1) {
                instancias.setClassIndex(instancias.numAttributes() - 1);
            } else {
                instancias.setClassIndex(i);
            }
        } catch (IllegalArgumentException e) {
            System.out.println("Indice de la clase no válido, tiene que estar entre 0 y " + (instancias.numAttributes() - 1) + " o ser -1");
        }
    }

    /**
     * Elimina un porcentaje de instancias del conjunto de instancias.
     *
     * @param perc       Porcentaje a eliminar.
     * @param inverse    En caso de estar a true se elimina (1-perc) de las instancias empezando por el final.
     * @param instancias Conjunto de instancias a las que se le va a aplicar el filtro.
     * @return Devuelve el conjunto de instancias que no han sido eliminadas.
     * @author Ander Cejudo
     */
    public static Instances removePercentage(Instances instancias, double perc, boolean inverse) {
        try {
            RemovePercentage rm = new RemovePercentage();
            rm.setInputFormat(instancias);
            rm.setInvertSelection(inverse);
            rm.setPercentage(perc);
            return Filter.useFilter(instancias, rm);
        } catch (Exception e) {
            System.out.println("No se ha podido aplicar el filtro remove percentage");
        }
        return null;
    }


    /**
     * Calcula la media dada una lista teniendo en cuenta que pueden existir valores NaN en ella.
     *
     * @param l La lista de números.
     * @return El valor medio de la lista.
     * @author Ander Cejudo
     */
    public static double media(double[] l) {
        double media = 0.0;
        int nan = 0;
        for (double d : l) {
            if (!Double.isNaN(d)) {
                media += d;
            } else {
                nan++;
            }
        }
        return media / (l.length - nan);
    }

    /**
     * Calcula la desviación típica de una lista teniendo en cuenta que pueden existir valores NaN en ella.
     *
     * @param l La lista de números.
     * @return La desviación típica de la lista.
     * @author Ander Cejudo
     */
    public static double desviacionTipica(double[] l) {
        double media = media(l);
        double dev = 0.0;
        int nan = 0;
        for (int i = 0; i < l.length; i++) {
            if (!Double.isNaN(l[i])) {
                dev += (l[i] - media) * (l[i] - media);
            } else {
                nan++;
            }
        }
        return ((double) Math.round(Math.sqrt(dev / (l.length - nan)) * 10000) / 10000);
    }

    /**
     * Elimina el archivo en la ruta especificada.
     *
     * @param path Ruta en la que se va a eliminar el archivo.
     */
    public static void eliminarArchivo(String path) {
        File f = new File(path);
        if (f.exists()) {
            //ABSOLUTO
            f.delete();
        } else {
            //RELATIVO
            String pwd = System.getProperty("user.dir");
            f = new File(pwd + "/" + path);
            if (f.exists()) {
                f.delete();
            } else {
                System.out.println("El archivo " + path + " no existe");
            }
        }
    }

    /**
     * Obtiene el índice de la clase que tiene el menor número de apariciones en todo el conjunto de datos proporcionado.
     *
     * @param instancias Conjunto del que se va a sacar el indice.
     * @return Un indice cuyo valor se encuentra entre [0,numValoresClase) y correponde al índice del conjunto de valores del atributo asignado como la clase.
     * @throws NullPointerException En caso de que el tipo del atributo correspondiente al índice no sea válido.
     * @author Ander Cejudo
     */
    public static int indiceClaseMinoritaria(Instances instancias) throws NullPointerException {
        return Utils.minIndex(instancias.attributeStats(instancias.classIndex()).nominalCounts);
    }

    /**
     * Le aplica el fitro AttributeSelection con el criterio InfoGain y el search Ranker al conjunto de instancias introducido.
     *
     * @param instancias   El conjunto de instancias al que se le desesa aplicar el filtro de seleccion de atributos.
     * @param numSelecStr  Numero de atributos que se quieren mantener como máximo. Esta en String, se transforma dentro, para tener en cuenta valores predefinidos.
     * @param thresholdStr Valor mínimo que tiene que tener un atributo para que se mantenga. Esta en String, se transforma dentro, para tener en cuenta valores predefinidos.
     * @return El conjunto de instancias con el filtro de seleccion de atributos aplicado.
     * @author Igor García
     */

    public static Instances seleccionarAtributos(Instances instancias, String numSelecStr, String thresholdStr) {
        //SE CREA EL FILTRO Y SE LE AÑADEN EL EVALUATOR Y EL RANKER
        AttributeSelection aS = new AttributeSelection();
        aS.setEvaluator(new InfoGainAttributeEval());
        //SE CREA EL RANKER
        Ranker ran = new Ranker();
        Instances filtradas = null;
        try {
            //SI EL PARÁMETRO DEL NUMERO DE ATRIBUTOS A SELECCIONAR ES DIFERENTE DE 'DEF', SE ESTABLECE EL VALOR ESPECIFICADO, EN CASO CONTRARIO, SE MANTIENE EL VALOR POR DEFECTO
            if (!numSelecStr.equals("DEF")) {
                Integer numSelec = Integer.parseInt(numSelecStr);
                ran.setNumToSelect(numSelec);
            }
            //SI EL PARÁMETRO DEL THRESHOLD ES DIFERENTE DE 'DEF', SE ESTABLECE EL VALOR ESPECIFICADO, EN CASO CONTRARIO, SE MANTIENE EL VALOR POR DEFECTO
            if (!(thresholdStr.equals("DEF"))) {
                ran.setThreshold(Double.parseDouble(thresholdStr));
            }
            //SE ESTABLECE EL RANKER RECIÉN CONFIGURADO COMO SEARCH DEL FILTRO
            aS.setSearch(ran);
            aS.setInputFormat(instancias);
            filtradas = Filter.useFilter(instancias, aS);
            //ASEGURAMOS QUE HAYA UN MÍNIMO DE 3 ATRIBUTOS
            if (filtradas.numAttributes() < 3) {
                //SI NO LO HAY SE VUELVE A LLAMAR AL MÉTODO OBLIGANDO A QUE HAYA 3 ATRIBUTOS
                seleccionarAtributos(instancias, "3", thresholdStr);
            } else {
                //EL FILTRO PONE LA CLASE AL FINAL
                filtradas.setClassIndex(filtradas.numAttributes() - 1);
            }
        } catch (Exception e) {
            System.out.println("No se ha podido aplicar el filtro de seleccion de parámetros");
        }
        return filtradas;
    }

    /**
     * Guarda el modelo dado en el directorio en el que se haya ejecutado.
     *
     * @param classifier El clasificador que se va a guardar.
     * @param ruta       Path absoluto en el que se va a guardar el modelo.
     * @author Ander Cejudo
     */
    public static void guardarModeloAbsoluto(Classifier classifier, String ruta) {
        try {
            SerializationHelper.write(ruta, classifier);
        } catch (Exception e) {
            System.out.println("No se ha podido guardar el modelo en " + ruta);
        }
    }

    /**
     * Guarda el modelo dado en el directorio en el que se haya ejecutado.
     *
     * @param classifier El clasificador que se va a guardar.
     * @author Ander Cejudo
     */
    public static void guardarModeloRelativo(Classifier classifier) {
        String pwd = System.getProperty("user.dir");
        try {
            SerializationHelper.write(pwd + "/SVM.model", classifier);
        } catch (Exception e) {
            System.out.println("No se ha podido guardar el modelo en " + pwd);
        }
    }

    /**
     * Se aplica el filtro FixedDictionatyStringToWordVector a las instancias introducidas con el diccionario que se establezca.
     *
     * @param test        El conjunto de instancias al que se le quiere aplicar el filtro.
     * @param diccionario String con las palabras que se quieren usar como atributos del Bag of Words.
     * @param tf          Indica si se va a utilizar la representación tf o bow.
     * @return El conjunto de instancias con el filtro del diccionario aplicado.
     * @author Igor García
     */
    public static Instances filtroDiccionario(Instances test, Object diccionario, boolean tf) {
        Instances testBoW = null;
        try {
            //SI TIENE STRING SE APLICARÁ EL FILTRO
            if (Utilidades.tieneAtributoString(test)) {
                FixedDictionaryStringToWordVector f = new FixedDictionaryStringToWordVector();
                //VER SI EL DICCIONARIO DADO ES UN STRING O UN FICHERO
                if (diccionario.getClass().getSimpleName().equals("File")) {
                    f.setDictionaryFile((File) diccionario);
                } else if (diccionario.getClass().getSimpleName().equals("String")) {
                    Reader r = new StringReader((String) diccionario);
                    f.setDictionarySource(r);
                }
                //CONFIGURAR Y APLICAR EL FILTRO
                f.setInputFormat(test);
                f.setTFTransform(tf);
                f.setIDFTransform(false);
                f.setOutputWordCounts(tf);
                testBoW = Filter.useFilter(test, f);
                testBoW.setClassIndex(0);
            } else {
                //SI NO TIENE STRING SE QUITAN LOS ATRIBUTOS MANUALMENTE
                String dict = (String) diccionario;
                String[] palabras = dict.split("\r\n");
                List<String> lista_palabras = Arrays.asList(palabras);
                for (int i = 0; i < test.numAttributes(); i++) {
                    if (!lista_palabras.contains(test.attribute(i).name()) && !test.attribute(i).name().equals(test.classAttribute().name())) {
                        test.deleteAttributeAt(i);
                        i--;
                    }
                }
                testBoW = new Instances(test);
            }
        } catch (Exception e) {
            System.out.println("No se ha podido aplicar el filtro FixedDictionaryStringToWordVector");
        }
        return testBoW;
    }

    /**
     * Carga el modelo desde la ruta indicada.
     *
     * @param ruta La ruta en la que se encuentra el modelo.
     * @return Devuelve el clasificador obtenido.
     */
    public static Classifier cargarModelo(String ruta) {
        try {
            return (Classifier) SerializationHelper.read(ruta);
        } catch (Exception e) {
            System.out.println("No se ha podido cargar el modelo de " + ruta);
        }
        return null;
    }

    /**
     *Transforma el conjunto de instancias introducido de disperso a no disperso
     *
     * @param data Conjunto de instancias que se quiere transformar
     * @return El mismo conjunto de entrada, pero no disperso.
     * @author Ander Cejudo
     */
    public static Instances nonSparseToSparse(Instances data) {
        try {
            NonSparseToSparse filtro = new NonSparseToSparse();
            filtro.setInputFormat(data);
            filtro.setTreatMissingValuesAsZero(true);
            return Filter.useFilter(data, filtro);
        } catch (Exception e) {
            System.out.println("No se ha podido aplicar el filtro NonSparseToSparse");
        }
        return null;
    }
}
