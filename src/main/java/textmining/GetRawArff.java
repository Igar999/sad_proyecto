package textmining;

import weka.core.Instances;
import weka.core.converters.CSVLoader;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.NominalToString;
import weka.filters.unsupervised.attribute.Remove;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Convierte los datos de un archivo .csv a un fichero .arff que son con los que trabaja la librería de weka.
 *
 * @author Aitor Javier Perez
 */
public class GetRawArff {
    public GetRawArff() {
    }

    /**
     * Ejecutable utilizado en GetRawArff.jar.
     *
     * @param args Los argumentos del programa.
     */
    public static void main(String[] args) {
        if (args.length != 2) {
            System.out.println("################################################################################################################################################");
            System.out.println(
                    "#                                       ____      _   ____                 _          __  __                                                   #\n" +
                            "#                                      / ___| ___| |_|  _ \\ __ ___      __/ \\   _ __ / _|/ _|                                                  #\n" +
                            "#                                     | |  _ / _ \\ __| |_) / _` \\ \\ /\\ / / _ \\ | '__| |_| |_                                                   #\n" +
                            "#                                     | |_| |  __/ |_|  _ < (_| |\\ V  V / ___ \\| |  |  _|  _|                                                  #\n" +
                            "#                                      \\____|\\___|\\__|_| \\_\\__,_| \\_/\\_/_/   \\_\\_|  |_| |_|                                                    #");
            System.out.println("#                                                                                                                                              #");
            System.out.println("################################################################################################################################################");
            System.out.println("# Objetivo: convertir el archivo train.csv al tipo de archivo ARFF.                                                                            #");
            System.out.println("# Pre-condición: archivo de formato .csv y con encoding Latin-1.                                                                               #");
            System.out.println("# Post-condición: se creará un archivo train_conv.csv y un archivo ARFF según se haya especificado.                                            #");
            System.out.println("# Argumentos:                                                                                                                                  #");
            System.out.println("#     1. Path del archivo de entrada con extensión .csv a convertir.                                                                           #");
            System.out.println("#     2. Path del archivo de salida con extensión .arff en el cual se guardarán las instancias.                                                #");
            System.out.println("# Ejemplos de uso:                                                                                                                             #");
            System.out.println("#     1. java -jar getRwARFF.jar /home/usuario/train.csv /home/usuario/trainTransformed.arff                                                   #");
            System.out.println("#     2. java -jar getRwARFF.jar train.csv /home/usuario/trainTransformed.arff                                                                 #\n" +
                    "#        (siempre que train.csv esté en el mismo directorio de ejecución)                                                                      #");
            System.out.println("################################################################################################################################################");
            return;
        }
        String path = "";
        String pwd = System.getProperty("user.dir");
        GetRawArff ra = new GetRawArff();
        File f = new File(pwd + args[0]);
        if (f.exists()) {
            if (args[1].split("/").length < 3) {
                path = pwd + "/" + args[0];
                Instances data = ra.loadCSV(path);
                Utilidades.guardarArffRelativo(ra.eliminarAtributo(data), pwd + "/" + args[1]);
            } else {
                path = pwd + "/" + args[0];
                Instances data = ra.loadCSV(path);
                Utilidades.guardarArffAbsoluto(ra.eliminarAtributo(data), args[1]);
            }
        } else {
            path = args[0];
            Instances data = ra.loadCSV(path);
            Utilidades.guardarArffAbsoluto(ra.eliminarAtributo(data), args[1]);
        }
        System.out.println("Convertido con éxito");
    }

    /**
     * Devuelve las Insances del CSV pasado, abriéndolo en el encoding adecuado.
     *
     * @param inputPath Es el path donde debería estar el train.csv.
     * @return El conjunto de instancias leidas del .csv.
     * @author Aitor Javier Perez
     */
    public Instances loadCSV(String inputPath) {
        String decodedPath = addSubStringToPath(inputPath, "decoded");
        try {
            Utilidades.escribirFicheroAbsoluto(getCompatibleText(inputPath), decodedPath);
            CSVLoader csvLoader = new CSVLoader();
            csvLoader.setFieldSeparator(",");
            csvLoader.setNoHeaderRowPresent(false);
            csvLoader.setSource(new File(decodedPath));
            return csvLoader.getDataSet();
        } catch (IOException e) {
            System.out.println("No se ha cargado el CSV correctamente, revisa el path...");
            System.exit(1);
        }
        return null;
    }

    /**
     * Devuelve un String del archivo pasado, abriéndolo en LATIN-1 (ISO-8859-1).
     * Reemplaza todos los símbolos por espacios.
     *
     * @param inputPath es el path donde debería estar el train.csv.
     * @return El texto leido de inputPath sin ciertos caracteres.
     * @author Aitor Javier Perez
     */
    private String getCompatibleText(String inputPath) {
        Path path = Paths.get(inputPath, new String[0]);
        String decoded;
        try {
            decoded = new String(Files.readAllBytes(path), StandardCharsets.ISO_8859_1);
            return decoded.replaceAll("['`?\"}{^_;:]", " ");
        } catch (IOException e) {
            System.out.println("Hay problemas en obtener el texto compatible...");
            System.exit(1);
        }
        return null;
    }

    /**
     * Añade una cadena de caracteres a un path dado.
     *
     * @param path      la cadena a la que se le va a añadir otra cadena.
     * @param substring la cadena que se añade al final.
     * @return Concatenación de path con substring.
     * @author Aitor Javier Perez
     */
    public String addSubStringToPath(String path, String substring) {
        String[] aux = path.split("/");
        String fileName = aux[aux.length - 1];
        aux[aux.length - 1] = fileName.substring(0, fileName.indexOf('.')) + "_" + substring + fileName.substring(fileName.indexOf('.'));
        return String.join("/", (CharSequence[]) aux);
    }

    /**
     * Elimina el atributo id del fichero train.csv y renombra el atributo nominal a class-attr junto al nombre de la relación words-to-process.
     *
     * @param data instancias de los datos extraidos del fichero train.csv ya tratados.
     * @return Elimina el atributo con el codigo del conjunto de instancias y establece como string el primer atributo.
     * @author Aitor Javier Perez
     */
    public Instances eliminarAtributo(Instances data) {
        Instances instNew;
        Remove remove = new Remove();
        remove.setAttributeIndices("1");
        try {
            remove.setInputFormat(data);
            instNew = Filter.useFilter(data, remove);
            NominalToString convert = new NominalToString();
            convert.setAttributeIndexes("first");
            convert.setInputFormat(instNew);
            instNew = Filter.useFilter(instNew, convert);
            instNew.setRelationName("words-to-process");
            instNew.renameAttribute(1, "class-attr");
            return instNew;
        } catch (Exception e) {
            System.out.println("No se ha eliminado el atributo correctamente...");
            System.exit(1);
        }
        return null;
    }
}
