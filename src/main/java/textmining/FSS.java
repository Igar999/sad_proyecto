package textmining;

import weka.core.Instances;

/**
 * Dado un conjunto de instancias (train) en formato Bag of Words o TF y otro conjunto en formato raw, aplica el filtro de seleccion de atributos al train y adapta el test al train con los atributos seleccionados.
 *
 * @author Igor García
 */
public class FSS {

    public FSS() {
    }

    /**
     * @param args Argumentos de entrada del programa, deben ser 7 para su correcto funcionamiento.
     * @author Igor García
     */
    public static void main(String[] args) {
        try {
            if (args.length == 0) {
                System.out.println("#######################################################################################################################################################################");
                System.out.println("#                                                                            _____ ____ ____                                                                          #\n" +
                        "#                                                                           |  ___/ ___/ ___|                                                                         #\n" +
                        "#                                                                           | |_  \\___ \\___ \\                                                                         #\n" +
                        "#                                                                           |  _|  ___) |__) |                                                                        #\n" +
                        "#                                                                           |_|   |____/____/                                                                         #\n" +
                        "#                                                                                                                                                                     #");
                System.out.println("#######################################################################################################################################################################");
                System.out.println("# Objetivo: transformar un archivo .arff a Bag of Words teniendo en cuenta los atributos de otro .arff tras pasar el filtro AttributeSelection.                       #");
                System.out.println("# Pre-condición: el argumento 1 no debe estar en formato BoW, el argumento 2 debe estar en formato raw.                                                               #");
                System.out.println("# Post-condición: en la ruta del argumento 3 se habrá creado un archivo con los mismos atributos que el argumento 1.                                                  #");
                System.out.println("# Argumentos:                                                                                                                                                         #");
                System.out.println("#     1. Ruta del archivo trainBoW en formato .arff que se quiere utilizar. Es el que define el espacio de atributos.                                                 #");
                System.out.println("#     2. Ruta del archivo test en formato .arff que se desea transformar a Bag of Words con los atributos seleccionados del trainBoW.                                 #");
                System.out.println("#     3. Ruta en la que se desea guardar el archivo trainBoWSelec, que tendrá los atributos seleccionados.                                                            #");
                System.out.println("#     4. Ruta en la que se desea guardar el archivo testBoWSelec, que tendrá los mismos atributos que trainBoW.                                                       #");
                System.out.println("#     5. Tf o bow.                                                                                                                                                    #");
                System.out.println("#     6. Número de atributos que se quieren mantener como máximo en el AttributeSelection. -1 significa mantenerlos todos.                                            #");
                System.out.println("#     7. Valor mínimo que tiene que presentar un atributo para que se mantenga. DEF significa usar el valor por defecto.                                              #");
                System.out.println("# Ejemplos:                                                                                                                                                           #");
                System.out.println("#     1. java -jar FSS.jar /home/usuario/trainBoW.arff /home/usuario/test.arff /home/usuario/trainBoWSelec.arff /home/usuario/testBoWSelec.arff bow 100 DEF           #");
                System.out.println("#     2. java -jar FSS.jar C:\\usuario\\trainBoW.arff C:\\usuario\\test.arff C:\\usuario\\trainBoWSelec.arff C:\\usuario\\testBoWSelec.arff t f 250 1                         #");
                System.out.println("#     3. java -jar FSS.jar trainBoW.arff test.arff trainBoWSelec testBoWSelec.arff bow 0.0000001 DEF (Usando paths relativos)                                         #");
                System.out.println("#######################################################################################################################################################################");
            } else if (args.length == 7) {
                boolean tf;
                //MIRANDO EL ARGUMENTO 5 SE DECIDE SI SE HARÁ EN BOW O EN TF
                if (args[4].equalsIgnoreCase("tf")) {
                    tf = true;
                } else if (args[4].equalsIgnoreCase("bow")) {
                    tf = false;
                } else {
                    throw new Exception();
                }
                //SE OBTIENEN LOS CONJUNTOS TRAINBOW Y TEST
                Instances trainBoW = Utilidades.archivo2Instancias(args[0]);
                Instances test = Utilidades.archivo2Instancias(args[1]);
                trainBoW.setClassIndex(0);
                //SE APLICA LA SELECCION DE ATRIBUTOS AL TRAINBOW TENIENDO EN CUENTA EL NUMERO DE ATRIBUTOS A MANTENER Y EL VALOR MINIMO PARA QUE UN ATRIBUTO SEA ELEGIDO
                Instances trainFSS = Utilidades.seleccionarAtributos(trainBoW, args[5], args[6]);
                Instances testFSS = null;
                //SI EL CONJUNTO DE TEST TIENE ATRIBUTO STRING, SE LE APLICA EL FILTRO FIXEDDICTIONARYSTRINGTOWORDVECTOR CON EL DICCIONARIO DE ATRIBUTOS SELECCIONADOS DEL TRAINBOW
                if (Utilidades.tieneAtributoString(test)) {
                    String dict = Utilidades.diccionario(trainFSS);
                    testFSS = Utilidades.filtroDiccionario(test, dict, tf);
                    Utilidades.sparseToNonSparse(testFSS);
                }
                else{
                    System.out.println("No hay atributos de tipo String");
                }
                //SE EXPORTAN EL TRAIN Y EL TEST CON LA SELECCION DE ATRIBUTOS DEL TRAIN
                Utilidades.exportar(trainFSS, args[2]);
                Utilidades.exportar(testFSS, args[3]);
            } else {
                System.out.println("Número incorrecto de parametros, ejecuta sin parametros para ver el funcionamiento.");
            }
        } catch (Exception e) {
            System.out.println("Los valores de los parámetros no son correctos");
        }
    }
}