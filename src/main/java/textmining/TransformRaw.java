package textmining;

import weka.core.Instances;
import weka.core.tokenizers.AlphabeticTokenizer;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.StringToWordVector;

import java.io.File;

/**
 * Procesa el atributo string del conjunto de entrenamiento mediante BoW o TF-IDF creando un diccionario (un atributo por palabra) en formato sparse o nonsparse.
 *
 * @author Ander Cejudo
 */

public class TransformRaw {

    /**
     * Contiene todas las instancias sobre las que se va a trabajar.
     */
    private Instances data;
    /**
     * Path del archivo en el que se encuentran todas las instancias.
     */
    private String pathO;
    /**
     * Path del archivo en el que se van a guardar todas las instancias procesadas.
     */
    private String pathD;

    /**
     * @param pO dirección del archivo del cual se cargarán las instancias.
     * @param pD dirección del archivo al cual se cargarán las instancias procesadas.
     * @author Ander Cejudo
     */
    public TransformRaw(String pO, String pD) {
        this.pathD = pD;
        this.pathO = pO;
    }

    /**
     * Ejecutable utilizado en textmining.TransformRaw.jar.
     *
     * @param args Los argumentos del programa.
     * @author Ander Cejudo
     */
    public static void main(String[] args) {
        try {
            if (args.length == 0) {
                System.out.println("################################################################################################################################################");
                System.out.println(
                        "#                                         _____                     __                      ____                                               #\n" +
                                "#                                        |_   _| __ __ _ _ __  ___ / _| ___  _ __ _ __ ___ |  _ \\ __ ___      __                               #\n" +
                                "#                                          | || '__/ _` | '_ \\/ __| |_ / _ \\| '__| '_ ` _ \\| |_) / _` \\ \\ /\\ / /                               #\n" +
                                "#                                          | || | | (_| | | | \\__ \\  _| (_) | |  | | | | | |  _ < (_| |\\ V  V /                                #\n" +
                                "#                                          |_||_|  \\__,_|_| |_|___/_|  \\___/|_|  |_| |_| |_|_| \\_\\__,_| \\_/\\_/                                 #");
                System.out.println("#                                                                                                                                              #");
                System.out.println("################################################################################################################################################");
                System.out.println("# Objetivo: convertir un atributo de tipo String en un vector de palabras y/o aplicar el filtro nonSparseToSparse sobre el archivo de entrada. #");
                System.out.println("# Pre-condición: como mucho habrá un atributo de tipo String.                                                                                  #");
                System.out.println("# Post-condición: un archivo .arff cuyo atributo String se habrá convertido en un vector de atributos y tendrá o no formato Sparse,            #" +
                        "\n# según se haya especificado.                                                                                                                  #");
                System.out.println("# Argumentos:                                                                                                                                  #");
                System.out.println("#     1. Path del archivo de entrada con extensión .arff del cual se cargarán las instancias.                                                  #");
                System.out.println("#     2. Path del archivo de salida con extensión .arff en el cual se guardarán las instancias.                                                #");
                System.out.println("#     3. Tf o bow.                                                                                                                             #");
                System.out.println("#     4. Formato del archivo de salida, sparse o nonsparse.                                                                                    #");
                System.out.println("# Ejemplos de uso:                                                                                                                             #");
                System.out.println("#     1. java -jar textmining.TransformRaw.jar /home/usuario/train.arff /home/usuario/trainTransformed.arff tf sparse.                         #");
                System.out.println("#     2. java -jar textmining.TransformRaw.jar /home/usuario/train.arff /home/usuario/trainTransformed.arff bow sparse.                        #");
                System.out.println("#     3. java -jar textmining.TransformRaw.jar /home/usuario/train.arff /home/usuario/trainTransformed.arff tf nonsparse.                      #");
                System.out.println("#     4. java -jar textmining.TransformRaw.jar /home/usuario/train.arff /home/usuario/trainTransformed.arff bow nonsparse.                     #");
                System.out.println("################################################################################################################################################");
            } else if (args.length == 4) {
                boolean tf, parse;
                //COMPROBACIÓN DE LOS PARÁMETROS DE ENTRADA
                if (args[2].equalsIgnoreCase("tf")) {
                    tf = true;
                } else if (args[2].equalsIgnoreCase("bow")) {
                    tf = false;
                } else {
                    throw new Exception();
                }
                if (args[3].contentEquals("sparse")) {
                    parse = false;
                } else if (args[3].contentEquals("nonsparse")) {
                    parse = true;
                } else {
                    throw new Exception();
                }
                //COMPRUEBA SI LOS ARCHIVOS DE ENTRADA USAN PATHS ABSOLUTOS O RELATIVOS
                String pwd = System.getProperty("user.dir");
                File f = new File(pwd + args[0]);
                TransformRaw tr;
                if (f.exists()) {
                    if (args[1].split("/").length < 3) {
                        tr = new TransformRaw(pwd + "/" + args[0], pwd + "/" + args[1]);
                    } else {

                        tr = new TransformRaw(pwd + "/" + args[0], args[1]);
                    }
                } else {
                    if (args[1].split("/").length < 3) {
                        tr = new TransformRaw(args[0], pwd + "/" + args[1]);
                    } else {
                        tr = new TransformRaw(args[0], args[1]);
                    }
                }
                tr.data = Utilidades.archivo2Instancias(tr.pathO);
                tr.StringToWordVector(tf);
                if (!parse) {
                    tr.data = Utilidades.sparseToNonSparse(tr.data);
                } else {
                    tr.data = Utilidades.nonSparseToSparse(tr.data);
                }
                Utilidades.guardarArffAbsoluto(tr.data, tr.pathD);
            } else {
                System.out.println("Este programa necesita 4 argumentos, ha recibido " + args.length);
            }
        } catch (Exception e) {
            System.out.println("Los valores de los parámetros no son correctos");
        }
    }

    /**
     * Método que convierte el atributo String del conjunto de datos en un vector de palabras, cuyos valores
     * variarán en función del método seleccionado (el valor del parámetro). El resultado del filtro se guardará
     * en el atributo {@link TransformRaw#data}.
     *
     * @param tf si es true se aplicará el tf y en caso contrario BoW.
     * @author Ander Cejudo
     */
    public void StringToWordVector(boolean tf) {
        try {
            String indices = "";
            boolean hayString = false;
            for (int i = 1; i <= data.numAttributes(); i++) {
                if (data.attribute(i - 1).isString()) {
                    indices += "" + i;
                    hayString = true;
                    break;
                }
            }
            if (hayString) {
                StringToWordVector filter = new StringToWordVector();
                filter.setDoNotOperateOnPerClassBasis(false);
                filter.setInvertSelection(false);
                filter.setMinTermFreq(0);
                filter.setAttributeIndices(indices);
                //TODAS LAS PALABRAS PASAN A MINÚSCULAS
                filter.setLowerCaseTokens(true);
                //PONEMOS UN NÚMERO MUY ALTO PARA QUE NO LIMITE
                filter.setWordsToKeep(1000000);
                // SI LA REPRESENTACIÓN ES TF, SE AÑADEN ESTAS OPCIONES
                filter.setIDFTransform(false);
                filter.setTFTransform(tf);
                //SI ES BOW 1/0, SI NO, EL NÚMERO DE APARICIONES
                filter.setOutputWordCounts(tf);
                filter.setTokenizer(new AlphabeticTokenizer());
                //Guardamos el diccionario para poder hacer el test o dev compatible
                String pwd = System.getProperty("user.dir");
                File f = new File(pwd + "/diccionario.txt");
                f.createNewFile();
                filter.setDictionaryFileToSaveTo(f);
                filter.setSaveDictionaryInBinaryForm(false);
                filter.setInputFormat(data);
                data = Filter.useFilter(data, filter);
                data.setClassIndex(0);
            }
        } catch (Exception e) {
            System.out.println("No se ha podido aplicar el filtro StringToWordVector");
        }
    }

}