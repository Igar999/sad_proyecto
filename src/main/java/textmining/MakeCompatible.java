package textmining;

import weka.core.Instances;

import java.io.File;

/**
 * Adapta el conjunto de instancias del test al espacio de atributos del conjunto de entrenamiento.
 *
 * @author Igor García
 */

public class MakeCompatible {

    public MakeCompatible() {
    }

    /**
     * Método main que se ejecuta para transformar el test al espacio de atributos del trainBoW.
     *
     * @param args 1.Ruta de fichero train BoW, 2. Ruta de fichero test, 3. Ruta de salida de testBoW.
     * @author Igor García
     */
    public static void main(String[] args) {
        try {
            if (args.length == 0) {
                System.out.println("################################################################################################################################################");
                System.out.println("#                                 __  __       _         ____                            _   _ _     _                                         #\n" +
                        "#                                |  \\/  | __ _| | _____ / ___|___  _ __ ___  _ __   __ _| |_(_) |__ | | ___                                    #\n" +
                        "#                                | |\\/| |/ _` | |/ / _ \\ |   / _ \\| '_ ` _ \\| '_ \\ / _` | __| | '_ \\| |/ _ \\                                   #\n" +
                        "#                                | |  | | (_| |   <  __/ |__| (_) | | | | | | |_) | (_| | |_| | |_) | |  __/                                   #\n" +
                        "#                                |_|  |_|\\__,_|_|\\_\\___|\\____\\___/|_| |_| |_| .__/ \\__,_|\\__|_|_.__/|_|\\___|                                   #\n" +
                        "#                                                                           |_|                                                                #");
                System.out.println("################################################################################################################################################");
                System.out.println("# Objetivo: transformar un archivo .arff a Bag of Words o tf teniendo en cuenta el espacio de atributos de otro .arff.                         #");
                System.out.println("# Pre-condición: el argumento 1 debe tener una palabra en cada línea, el argumento 2 debe estar en formato raw.                                #");
                System.out.println("# Post-condición: en la ruta del argumento 3 se habrá creado un archivo con los mismos atributos que el argumento 1.                           #");
                System.out.println("# Argumentos:                                                                                                                                  #");
                System.out.println("#     1. Ruta del archivo del diccionario en formato .txt que se quiere utilizar para determinar el espacio de atributos.                      #");
                System.out.println("#     2. Ruta del archivo test en formato .arff que se desea transformar a Bag of Words.                                                       #");
                System.out.println("#     3. Ruta en la que se desea guardar el archivo testBoW, que tendrá los mismos atributos que trainBoW.                                     #");
                System.out.println("#     4. Tf o bow.                                                                                                                             #");
                System.out.println("# Ejemplos:                                                                                                                                    #");
                System.out.println("#     1. java -jar MakeCompatible.jar /home/usuario/diccionario.txt /home/usuario/test.arff /home/usuario/testBoW.arff tf                      #");
                System.out.println("#     2. java -jar MakeCompatible.jar C:\\usuario\\diccionario.txt C:\\usuario\\test.arff C:\\usuario\\testBoW.arff bow                              #");
                System.out.println("#     3. java -jar MakeCompatible.jar diccionario.txt test.arff testBoW.arff bow (Usando paths relativos)                                      #");
                System.out.println("################################################################################################################################################");
            } else if (args.length == 4) {
                File diccionario;
                if (!args[0].contains("txt")) {
                    throw new IllegalArgumentException();
                }
                String pwd = System.getProperty("user.dir");
                File f = new File(pwd + args[0]);
                if (f.exists()) {
                    diccionario = new File(pwd + "/" + args[0]);
                } else {
                    diccionario = new File(args[0]);
                }
                boolean tf;
                if (args[3].equalsIgnoreCase("tf")) {
                    tf = true;
                } else if (args[3].equalsIgnoreCase("bow")) {
                    tf = false;
                } else {
                    throw new Exception();
                }
                Instances test = Utilidades.archivo2Instancias(args[1]);
                if (Utilidades.tieneAtributoString(test)) {
                    Instances testTransform = Utilidades.filtroDiccionario(test, diccionario, tf);
                    Utilidades.sparseToNonSparse(testTransform);
                    Utilidades.exportar(testTransform, args[2]);
                } else {
                    System.out.println("No hay ningún atributo String en ese conjunto");
                }
            } else {
                System.out.println("El número de parámetros no es correcto");
            }
        } catch (IllegalArgumentException i) {
            System.out.println("El diccionario no es válido");
        } catch (Exception e) {
            System.out.println("Los valores de los parámetros no son correctos");
        }
    }
}