package textmining;

import weka.classifiers.Classifier;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.evaluation.Evaluation;
import weka.core.Instances;

import java.text.DecimalFormat;
import java.util.Random;

/**
 * Obtiene un modelo utilizando el clasificador NaiveBayes a partir de los parámetros que maximizan la eficacia del modelo a partir del
 * conjunto de instancias proporcionado y saca su calidad estimada y lo guarda en un fichero.
 *
 * @author Aitor Javier Perez
 */
public class GetBaselineModel {
    /**
     * Indica el número de veces que se va a realizar k-fold cross validation.
     */
    private final static int NUM_ITERACIONES = 1;
    /**
     * La forma en la que se representará el espacio de atributos en caso de haber algún String.
     */
    private final static String REPRESENTACION = "bow"; // tf o bow
    /**
     * Path y archivo donde se va a guardar el modelo.
     */
    private static String pathModelo;
    /**
     * Path y archivo donde se va a guardar la calidad del modelo.
     */
    private static String pathCalidad;
    /**
     * Path donde está el arff pasado por parámetros.
     */
    private static String pathdatoscompletos;
    /**
     * Las instancias que aparecen en el archivo pasado por parámetros.
     */
    private Instances datoscompletos;
    /**
     * Contador utilizado para calcular el progreso en la optimización de parámetros.
     */
    private int cont;

    /**
     * @param pPathDC      Path del los datos completos, del archivo que se pasa por parámetro.
     * @param pPathModelo  Path donde se va a guardar el modelo.
     * @param pPathCalidad Path donde se va a guardar el archivo de calidad.
     */
    public GetBaselineModel(String pPathDC, String pPathModelo, String pPathCalidad) {
        pathdatoscompletos = pPathDC;
        pathModelo = pPathModelo;
        pathCalidad = pPathCalidad;
    }

    /**
     * Metodo ejecutable para crear el jar GetBaselineModel.jar.
     *
     * @param args Lista de parámetros.
     * @author Aitor Javier Perez
     */
    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("################################################################################################################################################");
            System.out.println(
                    "#                                     ____      _   ____                 _ _            __  __           _      _                              #\n" +
                            "#                                    / ___| ___| |_| __ )  __ _ ___  ___| (_)_ __   ___|  \\/  | ___   __| | ___| |                             #\n" +
                            "#                                   | |  _ / _ \\ __|  _ \\ / _` / __|/ _ \\ | | '_ \\ / _ \\ |\\/| |/ _ \\ / _` |/ _ \\ |                             #\n" +
                            "#                                   | |_| |  __/ |_| |_) | (_| \\__ \\  __/ | | | | |  __/ |  | | (_) | (_| |  __/ |                             #\n" +
                            "#                                    \\____|\\___|\\__|____/ \\__,_|___/\\___|_|_|_| |_|\\___|_|  |_|\\___/ \\__,_|\\___|_|                             #");
            System.out.println("#                                                                                                                                              #");
            System.out.println("################################################################################################################################################");
            System.out.println("# Objetivo: obtener un modelo NaiveBayes para poder clasificar las instancias del arff pasado.                                                 #");
            System.out.println("# Pre-condición: la clase no puede ser numérica.                                                                                               #");
            System.out.println("# Post-condición: se creará un archivo con el nombre del modelo y un fichero de texto con la calidad estimada del NaiveBayes.                  #");
            System.out.println("# Argumentos:                                                                                                                                  #");
            System.out.println("#     1. Archivo arff de las instancias del train.                                                                                             #");
            System.out.println("#     2. Indice de la clase del arff, sino se pone nada por defecto será 0.                                                                    #");
            System.out.println("#     3. Path del modelo NaiveBayes.										                                                                   #");
            System.out.println("#     4. Archivo de la calidad estimada de NaiveBayes.					                                                                       #");
            System.out.println("# Ejemplos de uso:                                                                                                                             #");
            System.out.println("#     1. java -jar GetBaselineModel.jar /home/usuario/train.arff 2 /path/to/destination/NaiveBayes.model                                       #");
            System.out.println("#     	/path/to/destination/NaiveBayes_CalidadEstimada.txt                                                                                    #");
            System.out.println("#     2. java -jar GetBaselineModel.jar /home/usuario/train.arff /path/to/destination/NaiveBayes.model                                         #");
            System.out.println("#     	/path/to/destination/NaiveBayes_CalidadEstimada.txt                                                                                    #");
            System.out.println("################################################################################################################################################");
            //VERIFICAMOS QUE SOLO HAY 3 O 4 ATRIBUTOS
        } else if (args.length == 3 || args.length == 4) {
            GetBaselineModel model;
            int index = 0;
            model = new GetBaselineModel(args[0], args[1], args[2]);
            //SI HAY 4, SE COJE EL SEGUNDO PARÁMETRO COMO ÍNDICE
            if (args.length == 4) {
                index = Integer.parseInt(args[1]);
                model = new GetBaselineModel(args[0], args[2], args[3]);
            }
            Stopwatch sw = new Stopwatch();
            try {
                //SE CARGAN LAS INSTANCIAS DEL ARCHIVO ARFF
                model.setInstances(Utilidades.archivo2Instancias(args[0]), index);
            } catch (Exception e) {
                System.out.println("Puede ser que el path del archivo no sea el correcto...");
            }
            //SI TIENE ATRIBUTO DE TIPO STRING, SE COJE
            if (Utilidades.tieneAtributoString(Utilidades.archivo2Instancias(pathdatoscompletos))) {
                //LA REPRESENTACIÓN QUE SE HAYA INDICADO Y SE CARGA EN LA CLASE TRANSFORMRAW
                String pwd = System.getProperty("user.dir");
                String[] args2 = {args[0], "train_" + model.getRepresentacion() + ".arff", model.getRepresentacion(), "nonsparse"};
                TransformRaw.main(args2);
                String npath = pwd + "/train_" + model.getRepresentacion() + ".arff";
                model.setInstances(Utilidades.archivo2Instancias(npath), 0);
            }
            System.out.print("                                                       \r");
            System.out.print("Creando modelo predictor...\r");
            Classifier nb = model.obtenerModelo();
            try {
                nb.buildClassifier(model.datoscompletos);
            } catch (Exception e) {
                System.out.println("No se ha podido entrenar el modelo!");
            }
            System.out.print("                                                       \r");
            System.out.print("Guardando modelo predictor y calidad estimada...\r");
            Utilidades.guardarModeloAbsoluto(nb, pathModelo);
            Utilidades.escribirFicheroAbsoluto(model.calidadEstimada() + "\nTiempo: " + sw.elapsedTime() + "s", pathCalidad);
            System.out.print("                                                       \r");
            System.out.println("El proceso ha tardado " + sw.elapsedTime() + " segundos");
            System.out.print("Terminado con éxito!\n");
        }
    }

    /**
     * Se fijan las instanciasen la clase {@link GetBaselineModel#datoscompletos} y se fija el índice también.
     *
     * @param attr  Instancias a asignar en {@link GetBaselineModel#datoscompletos}.
     * @param index Índice a asignar {@link GetBaselineModel#datoscompletos}.
     * @author Aitor Javier Perez
     */
    private void setInstances(Instances attr, int index) {
        datoscompletos = attr;
        Utilidades.setIndiceClase(index, attr);
    }

    /**
     * Se obtiene la representación utilizada para los Strings.
     *
     * @return El tipo de representación.
     * @author Ander Cejudo
     */
    public String getRepresentacion() {
        return REPRESENTACION;
    }

    /**
     * Estima la calidad del modelo utilizando el esquema de evaluación k-fold cross validation y la clase minoritaria.
     *
     * @return Devuelve el texto con toda la información relativa a la calidad del modelo, el archivo utilizado y el valor de la clase minoritaria.
     * @author Ander Cejudo
     */
    private String calidadEstimada() {
        try {
            int indiceMinoritario = Utilidades.indiceClaseMinoritaria(datoscompletos);
            //K-FOLD CROSS VALIDATION (COTA REALISTA)
            Evaluation evaluator;
            double[] fmeasures = new double[NUM_ITERACIONES], recalls = new double[NUM_ITERACIONES], precisions = new double[NUM_ITERACIONES];
            for (int i = 0; i < NUM_ITERACIONES; i++) {
                imprimirProgreso();
                evaluator = new Evaluation(datoscompletos);
                double valRandom = Math.random() * 2001;
                evaluator.crossValidateModel(obtenerModelo(), datoscompletos, 3, new Random((long) valRandom));
                fmeasures[i] = evaluator.weightedFMeasure();
                recalls[i] = evaluator.weightedRecall();
                precisions[i] = evaluator.weightedPrecision();
            }
            //NO HONESTA (COTA SUPERIOR)
            Classifier nb2 = obtenerModelo();
            nb2.buildClassifier(datoscompletos);
            evaluator = new Evaluation(datoscompletos);
            evaluator.evaluateModel(nb2, datoscompletos);
            double fmeasureS = evaluator.weightedFMeasure();
            double reacallS = evaluator.weightedRecall();
            double precisionS = evaluator.weightedPrecision();
            DecimalFormat df = new DecimalFormat("#.####");
            String res = "------------CALIDAD ESTIMADA NAIVEBAYES------------";
            res += "\nArchivo: " + pathdatoscompletos;
            res += "\nClase minoritaria: " + datoscompletos.classAttribute().value(indiceMinoritario);
            res += "\nCota realista: ";
            res += "\n    - Error: " + df.format(Utilidades.desviacionTipica(fmeasures));
            res += "\n    - F-measure: " + df.format(Utilidades.media(fmeasures));
            res += "\n    - Recall: " + df.format(Utilidades.media(recalls));
            res += "\n    - Precision: " + df.format(Utilidades.media(precisions));
            res += "\nCota superior: ";
            res += "\n    - F-measure: " + df.format(fmeasureS);
            res += "\n    - Recall: " + df.format(reacallS);
            res += "\n    - Precision: " + df.format(precisionS);
            return res;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("No se ha podido obtener la calidad estimada");
            System.exit(1);
        }
        return "";
    }

    /**
     * Este método obtiene un modelo NaiveBayes para clasificar el conjunto de instancias
     * que se reciben.
     *
     * @return Modelo NaiveBayes.
     */
    private NaiveBayes obtenerModelo() {
        try {
            NaiveBayes nb = new NaiveBayes();
            if (datoscompletos.classAttribute().isNominal()) {
                nb.setUseKernelEstimator(false);
                nb.setUseSupervisedDiscretization(true);
            } else if (datoscompletos.classAttribute().isNumeric()) {
                nb.setUseKernelEstimator(true);
                nb.setUseSupervisedDiscretization(false);
            }
            return nb;
        } catch (Exception e) {
            System.out.println("Fallo al obtener el modelo...");
            System.exit(1);
        }
        return null;
    }

    /**
     * Imprime por pantalla el progreso de la optimización de parámetros.
     *
     * @author Ander Cejudo
     */
    public void imprimirProgreso() {
        DecimalFormat df2 = new DecimalFormat("###.##"); //INDICADOR DEL PROGRESO
        System.out.print("                                                       \r");
        if (cont < NUM_ITERACIONES) {
            System.out.print(df2.format(((double) cont / NUM_ITERACIONES) * 100) + "% completado\r");
            cont++;
        } else {
            //A VECES NO SE PUEDE CONTROLAR QUE SE SUPERE EL 100%, POR ESO LA CONDICIÓN
            System.out.print("100%\r");
        }
    }
}