package textmining;

import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.evaluation.output.prediction.PlainText;
import weka.core.Attribute;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.RenameNominalValues;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Dado un conjunto de test sin clasificar y un modelo clasificador, devuelve el conjunto clasificado.
 *
 * @author Igor García
 */
public class Predictions {

    public Predictions() {
    }

    /**
     * Método main que ejecuta las instrucciones de la clase.
     *
     * @param args Los argumentos que recibe el programa, deben ser 4 para su correcto funcionamiento.
     * @author Igor García
     */
    public static void main(String[] args) {
        try {
            if (args.length == 0) {
                System.out.println("#########################################################################################################################################################################");
                System.out.println("#                                                           ____               _ _      _   _                                                                           #\n" +
                        "#                                                          |  _ \\ _ __ ___  __| (_) ___| |_(_) ___  _ __  ___                                                           #\n" +
                        "#                                                          | |_) | '__/ _ \\/ _` | |/ __| __| |/ _ \\| '_ \\/ __|                                                          #\n" +
                        "#                                                          |  __/| | |  __/ (_| | | (__| |_| | (_) | | | \\__ \\                                                          #\n" +
                        "#                                                          |_|   |_|  \\___|\\__,_|_|\\___|\\__|_|\\___/|_| |_|___/                                                          #\n" +
                        "#                                                                                                                                                                       #");
                System.out.println("#########################################################################################################################################################################");
                System.out.println("# Objetivo: predecir la clase de las instancias de un conjunto de datos dado un modelo previamente entrenado.                                                           #");
                System.out.println("# Pre-condición: la clase no debe ser numérica.                                                                                                                         #");
                System.out.println("# Post-condición: en la ruta del argumento 2 se habrá creado el archivo con las predicciones de las clases y un arff con las predicciones.                              #");
                System.out.println("# Argumentos:                                                                                                                                                           #");
                System.out.println("#     1. Ruta del archivo train en formato raw, del que se sacarán los valores de la clase.                                                                             #");
                System.out.println("#     2. Ruta del archivo test en formato raw, que se devolverá clasificado.                                                                                            #");
                System.out.println("#     3. Ruta del modelo en formato .model que se usará para clasificar las instancias del conjunto de datos.                                                           #");
                System.out.println("#     4. Ruta en la que se desea guardar el archivo testClas, que tendrá las instancias clasificadas.                                                                   #");
                System.out.println("#     5. Índice de la clase en el documento de train y test raw (empezando en 0).                                                                                       #");
                System.out.println("# Ejemplos:                                                                                                                                                             #");
                System.out.println("#     1. java -jar Predictions.jar /home/usuario/train.arff /home/usuario/test.arff /home/usuario/modelo.model /home/usuario/testClas.arff 0                            #");
                System.out.println("#     2. java -jar Predictions.jar C:\\usuario\\train.arff C:\\usuario\\test.arff  C:\\usuario\\modelo.model C:\\usuario\\testClas.arff -1                                      #");
                System.out.println("#     3. java -jar Predictions.jar train.arff test.arff modelo.model testClas.arff 0 (Usando paths relativos)                                                           #");
                System.out.println("#########################################################################################################################################################################");
            } else if (args.length == 5) {
                Predictions p = new Predictions();
                Instances train = Utilidades.archivo2Instancias(args[0]);
                Instances testRaw = Utilidades.archivo2Instancias(args[1]);
                //SE ESTABLECE EL ÍNDICE DE LA CLASE
                if (args[4].equals("-1")) {
                    train.setClassIndex(train.numAttributes() - 1);
                    testRaw.setClassIndex(testRaw.numAttributes() - 1);
                } else {
                    train.setClassIndex(Integer.parseInt(args[4]));
                    testRaw.setClassIndex(Integer.parseInt(args[4]));
                }
                p.adaptarClase(testRaw,train);
                Instances test = null;
                //SI EL TEST TIENE ALGUN STRING SE ADAPTA EL TEST AL TRAIN
                if (Utilidades.tieneAtributoString(testRaw)) {
                    String[] args2 = {args[0], "train_BoW.arff", "bow", "nonsparse"};
                    //AQUÍ OBTENDREMOS BOW DEL TRAIN
                    TransformRaw.main(args2);
                    //CARGAMOS EL CONJUNTO DE ENTRENAMIENTO
                    train = Utilidades.archivo2Instancias("train_BoW.arff");
                    //LOS FILTROS CAMBIAN LA CLAtestVacioSE A LA POSICIÓN 0
                    Utilidades.setIndiceClase(0, train);
                    String diccionario = Utilidades.diccionario(train);
                    test = Utilidades.filtroDiccionario(testRaw, diccionario, false);
                } else {
                    test = testRaw;
                }
                //SE CARGA EL MODELO
                Classifier estimador = Utilidades.cargarModelo(args[2]);
                //PARA CADA UNA DE LAS INSTANCIAS, SE CLASIFICA EN EL BOW (PORQUE EL MODELO ESTÁ HECHO PARA EL BOW) Y SE COLOCA LA CLASE ESTIMADA EN EL RAW, PARA QUE QUEDE CLASIFICADO EL ARCHIVO ORIGINAL
                for (int i = 0; i < testRaw.numInstances(); i++) {
                    double clase = estimador.classifyInstance(test.instance(i));
                    testRaw.instance(i).setClassValue(clase);
                }
                //SE GUARDAN LAS PREDICCIONES EN UN FICHERO .TXT
                p.exportarPredicciones(test, estimador);
                //SE GUARDAN LOS RESULTADOS EN UN FICHERO .TXT
                p.exportarResultados(test, estimador);
                //SE GUARDA EL TEST.ARFF CON LAS INSTANCIAS CLASIFICADAS
                Utilidades.exportar(testRaw, args[3]);
                //SE ELIMINAN LAS SALIDAS INNECESARIAS
                p.eliminarArchivosGenerados();
                System.out.println("Terminado con exito!");
            } else {
                System.out.println("El número de parámetros no es correcto");
            }
        } catch (NullPointerException n){
            System.out.println("El modelo no ha sido capaz de clasificar algunas instancias");
            System.out.println("Puede que el modelo no corresponda al conjunto");
        } catch(Exception e) {
            System.out.println("Los parámetros no están bien");
        }
    }

    /**
     * Cambia el atributo de la clase del test a nominal, aunque ya lo sea, y pone los valores nominales en el mismo orden del
     * train. Al hacer esto se pierden los valores para la clase de cada instancia, por eso se copian todos.
     *
     * @param test Conjunto al que se le va a adaptar la clase.
     * @param train Conjunto del que se va a obtener la clase.
     * @author Ander Cejudo
     */
    public void adaptarClase(Instances test,Instances train) {
        ArrayList<String> values = new ArrayList<>();
        Instances aux = new Instances(test);
        //OBTIENE LOS VALORES DEL CONJUNTO DE ENTRENAMIENTO
        for (int i = 0; i < train.classAttribute().numValues(); i++){
            values.add(train.classAttribute().value(i));
        }
        //CREA LA CLASE
        Attribute clase_att = new Attribute(train.classAttribute().name(), values);
        //AÑADE LA CLASE AL TEST
        test.replaceAttributeAt(clase_att,test.classIndex());
        //VUELVE A COLOCAR LOS VALORES DE LA CLASE A LAS INSTANCIAS PARA EL NUEVO ATRIBUTO
        for (int j = 0;j < test.numInstances(); j++){
            String clase = aux.classAttribute().value((int)aux.instance(j).classValue());
            test.instance(j).setClassValue(test.classAttribute().indexOfValue(clase));
        }
    }

    /**
     * Exporta las predicciones a un archivo .txt, en el que aparecerá el id de la instancia, la clase real, la clase estimada y la distribución.
     *
     * @param sinClasificar Conjunto de instancias que no se han clasificado.
     * @param clasificador  Modelo encargado de clasificar las instancias.
     * @author Ander Cejudo
     */
    public void exportarPredicciones(Instances sinClasificar, Classifier clasificador) {
        String pwd = System.getProperty("user.dir");
        try {
            PlainText salida = new PlainText();
            salida.setHeader(sinClasificar);
            salida.setBuffer(new StringBuffer());
            salida.setOutputFile(new File(pwd + "/predicciones.txt"));
            salida.setOutputDistribution(true);
            salida.print(clasificador, sinClasificar);
        } catch (Exception e) {
            System.out.println("Error exportando las prediciones a " + pwd);
        }
    }

    /**
     * Elimina todos los archivos generados durante la ejecución del programa los cuales son necesarios para el funcionamiento del mismo.
     *
     * @author Ander Cejudo
     */
    public void eliminarArchivosGenerados() {
        Utilidades.eliminarArchivo("train_BoW.arff");
        Utilidades.eliminarArchivo("diccionario.txt");
    }

    /**
     * Saca los resultados de las predicciones al igual que la gui de weka, mostrando las metricas, la matriz de confusión y el porcentaje de acierto y de error.
     *
     * @param sinClasificar Conjunto de test sin clasificar.
     * @param clasificador El modelo.
     * @author Ander Cejudo
     */
    public void exportarResultados(Instances sinClasificar, Classifier clasificador) {
        String pwd = System.getProperty("user.dir");
        try {
            Evaluation e = new Evaluation(sinClasificar);
            e.evaluateModel(clasificador,sinClasificar);
            BufferedWriter bw = new BufferedWriter(new FileWriter(pwd + "/resultados.txt"));
            bw.write(e.toSummaryString());
            bw.newLine();
            bw.newLine();
            bw.write(e.toClassDetailsString());
            bw.newLine();
            bw.newLine();
            bw.write(e.toMatrixString());
            bw.flush();
            bw.close();
        } catch (Exception e) {
            System.out.println("Error exportando las prediciones a " + pwd);
        }
    }
}