package textmining;


import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.functions.LibSVM;
import weka.core.Instances;
import weka.core.SelectedTag;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Random;

/**
 * Obtiene un modelo utilizando el clasificador SVM a partir de los parámetros que maximizan la eficacia del modelo a partir del
 * conjunto de instancias proporcionado y saca su calidad estimada y lo guarda en un fichero.
 *
 * @author Ander Cejudo
 */
public class GetSVMModel {
    /**
     * Nombre de los parámetros del clasificador que se utilizan.
     */
    private final static String[] nombreParams = {"Coste (c)", "Exponente (degree)", "Gamma"};
    /**
     * Nombre de todos los kernels que se utilizan.
     */
    private final static String[] nombreKernels = {"Función lineal", "Función polinómica", "Función de base radial", "Función sigmoid"};
    /**
     * Indica el número de veces que se va a realizar hold-out y la k de k-fold cross validation.
     */
    private final static int NUM_ITERACIONES = 500;
    /**
     * La forma en la que se representará el espacio de atributos en caso de haber algun String.
     */
    private final static String REPRESENTACIÓN = "tf"; //tf o bow
    /**
     * El número de atributos que se van a utilizar al aplicar FSS
     */
    private final static String CUANTOS_ATRIBUTOS = "-1"; //DEF QUITA LOS QUE QUIERA EL FILTRO, -1 MANTIENE TODOS, EJEMPLO : 0.5
    /**
     * El número de operaciones a realizar en la optimización de parámetros, utilizado para calcular el progreso.
     */
    private static int NUMERO_OPERACIONES;
    /**
     * Almacena el path del archvio .arff del cual se van a obtener las instancias.
     */
    private String pathO;
    /**
     * Cotiene el conjunto de instancias obtenidas de {@link #pathO}.
     */
    private Instances data;
    /**
     * Contiene un subconjunto de instancias obtenidas a partir de {@link #data} para entrenar el modelo.
     */
    private Instances train;
    /**
     * Contiene un subconjunto de instancias obtenidas a partir de {@link #data} para evaluar el modelo.
     */
    private Instances test;
    /**
     * Almacena el valor F-measure más alto que se ha conseguido.
     */
    private double fOpt;
    /**
     * Contiene todas las listas con todos los posibles valores para cada uno de los parámetros.
     */
    private ArrayList<double[]> valoresParams;
    /**
     * Contador utilizado para calcular el progreso en la optimización de parámetros.
     */
    private int cont;
    /**
     * Posición en la que se encuentra la clase dentro del conjunto dado.
     */
    private int indiceClase;

    /**
     * @param pO Path del .arff a analizar.
     * @author Ander Cejudo
     */
    public GetSVMModel(String pO) {
        this.pathO = pO;
        this.fOpt = 0.0;
        double[] gamma = {0.1, 1, 10, 100};
        double[] c = {0.1, 1, 10, 1000};
        double[] degree = {0, 2, 3, 4, 5};
        this.valoresParams = new ArrayList<>();
        valoresParams.add(c);
        valoresParams.add(degree);
        valoresParams.add(gamma);
        this.cont = 0;
        NUMERO_OPERACIONES = nombreKernels.length * 4 * NUM_ITERACIONES;
    }

    /**
     * Metodo ejecutable para crear el jar.
     *
     * @param args Lista de parámetros.
     * @author Ander Cejudo
     */
    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("################################################################################################################################################");
            System.out.println("#                                                  ____      _   ______     ____  __ __  __           _      _                                 #\n" +
                    "#                                                 / ___| ___| |_/ ___\\ \\   / /  \\/  |  \\/  | ___   __| | ___| |                                #\n" +
                    "#                                                | |  _ / _ \\ __\\___ \\\\ \\ / /| |\\/| | |\\/| |/ _ \\ / _` |/ _ \\ |                                #\n" +
                    "#                                                | |_| |  __/ |_ ___) |\\ V / | |  | | |  | | (_) | (_| |  __/ |                                #\n" +
                    "#                                                 \\____|\\___|\\__|____/  \\_/  |_|  |_|_|  |_|\\___/ \\__,_|\\___|_|                                #\n" +
                    "#                                                                                                                                              #");
            System.out.println("################################################################################################################################################");
            System.out.println("# Objetivo: obtener el modelo a partir de los parámetros óptimos del conjunto de entrenamiento dado para el clasificador SVM utilizando.       #");
            System.out.println("# hold-out y después calcular la calidad estimada y guardar tanto el modelo como la calidad estimada.                                          #");
            System.out.println("# Pre-condición: La clase no debe ser numérica.                                                                                                #");
            System.out.println("# Post-condición: los parámetros óptimos y su F-measure, el modelo (SVM.model) y calidad estimada del modelo (SVM_CalidadEstimada.txt) en el   #");
            System.out.println("# directorio en el que se ha ejecutado el programa. En el archivo de la calidad estimada se proporciona además, el valor de la clase           #");
            System.out.println("# minoritaria, el kernel utilizado, el segundo parámetro y su valor y el archivo utilizado.                                                    #");
            System.out.println("# Argumentos:                                                                                                                                  #");
            System.out.println("#     1. Conjunto de instancias en formato .arff.                                                                                              #");
            System.out.println("#     2. Posición del atributo en la que está la clase (por defecto 0), si se pone -1, se cogerá el último atributo.                           #");
            System.out.println("# Ejemplos de uso:                                                                                                                             #");
            System.out.println("#      1. java -jar ParamOptimization.jar train.arff                                                                                           #");
            System.out.println("#      2. java -jar ParamOptimization.jar /home/usuario/train.arff                                                                             #");
            System.out.println("#      3. java -jar ParamOptimization.jar /home/usuario/train.arff 2                                                                           #");
            System.out.println("################################################################################################################################################");
        } else if (args.length == 1 || args.length == 2) {
            int indiceClase = 0;
            //SI SE HA INTRODUCIDO EL ÍNDICE DE LA CLASE
            if (args.length == 2) {
                try {
                    indiceClase = Integer.parseInt(args[1]);
                } catch (NumberFormatException e) {
                    System.out.println("Introduce un número válido");
                }
            }
            //PATH DESDE EL QUE SE HA EJECUTADO EL PROGRAMA
            String pwd = System.getProperty("user.dir");
            File f = new File(pwd + args[0]);
            GetSVMModel model;
            String path = "";
            //PATH RELATIVO?
            if (f.exists()) {
                path = pwd + "/" + args[0];
                model = new GetSVMModel(path);
            }
            //PATH ABSOLUTO
            else {
                path = args[0];
                model = new GetSVMModel(args[0]);
            }
            try {
                //EMPIEZA A CONTARSE EL TIEMPO DE EJECUCIÓN
                Stopwatch sw = new Stopwatch();
                //CARGAMOS INSTANCIAS Y OBTEEMOS LOS MEJORES PARÁMETROS
                model.data = Utilidades.archivo2Instancias(model.pathO);
                Utilidades.setIndiceClase(indiceClase, model.data);
                model.indiceClase = model.data.classIndex();
                AbstractMap.SimpleEntry<Integer, Double> mejoresParam = model.mejoresParametros();
                //SI TIENE ALGÚN ATRIBUTO STRING PASAMOS EL RAW.ARFF A BOW
                if (Utilidades.tieneAtributoString(model.data)) {
                    String[] args2 = {path, "train_" + model.getRepresentacion() + ".arff", model.getRepresentacion(), "nonsparse"};
                    TransformRaw.main(args2);
                    model.setPathO(pwd + "/train_" + model.getRepresentacion() + ".arff");
                    //LOS FILTROS UTILZADOS EN TRANSFORMRAW CAMBIAN LA CLASE A LA POSICIÓN 0
                    Utilidades.setIndiceClase(0, model.data);
                } else {
                    //AL NO APLICARSE NINGÚN FILTRO LA CLASE ESTÁ EN LA UBICACIÓN ESPECIFICADA EN EL ARGUMENTO
                    Utilidades.setIndiceClase(indiceClase, model.data);
                }
                model.indiceClase = model.data.classIndex();
                //VOLVEMOS A CARGAR LOS DATOS
                model.data = Utilidades.archivo2Instancias(model.pathO);
                Utilidades.setIndiceClase(model.indiceClase, model.data);
                //REDUCIMOS EL ESPACIO DE ATRIBUTOS SEGUN SE HAYA ESPECIFICADO
                System.out.print("                                                       \r");
                System.out.print("Creando modelo predictor...\r");
                //AHORA QUE HEMOS CARGADO EL CONJUNTO SIN STRINGS Y TENEMOS LOS PARÁMETROS ÓPTIMOS, OBTENEMOS EL MODELO
                Classifier classifier = model.crearModeloPredictor(mejoresParam.getKey(), mejoresParam.getValue());
                System.out.print("                                                       \r");
                System.out.print("Calculando calidad estimada...\r");
                //EVALUAMOS EL MODELO Y SACAMOS SU CALIDAD ESTIMADA
                String calidadEstimada = model.calidadEstimada(classifier);
                //AÑADIMO PARÁMETROS ÓPTIMOS Y TIEMPO DE EJUCIÓN A CONTENIDO DE LA CALIDAD ESTIMADA
                calidadEstimada += "\nParámetros utilizados: ";
                calidadEstimada += "\n    - Kernel: " + nombreKernels[mejoresParam.getKey()];
                calidadEstimada += "\n    - " + nombreParams[model.selIdx(mejoresParam.getKey())] + ": " + mejoresParam.getValue();
                System.out.print("                                                       \r");
                System.out.print("Guardando modelo predictor y calidad estimada...\r");
                //SE OBTIENE EL TIEMPO QUE HA PASADO DESDE QUE SE INSTANCIÓ LA CLASE
                double tiempo = sw.elapsedTime();
                calidadEstimada += "\nTiempo de ejecución: " + tiempo + "s";
                //GUARDAMOS LA CALIDAD Y EL MODELO
                Utilidades.escribirFicheroRelativo(calidadEstimada, "calidad-estimada.txt");
                Utilidades.guardarModeloRelativo(classifier);
                //ELIMINAMOS LOS ARCHIVOS GENERADOS DURANTE EL PROCESO
                model.eliminarArchivosGenerados();
                System.out.print("                                                       \r");
                System.out.print("Terminado con éxito!\n");
                System.out.println("Tiempo de ejecución: " + tiempo + "s");
            } catch (IOException i) {
                System.out.println("No se ha podido cargar el fichero, revisa la ruta");
            } catch (ArrayIndexOutOfBoundsException u) {
                System.out.println("El conjunto es muy pequeño para obtener los parámetros óptimos");
            } catch (IllegalArgumentException il) {
                System.out.println("Formato del archivo no válido");
            } catch (NullPointerException | IndexOutOfBoundsException i) {
                System.out.println("El índice para la clase no es válido");
            } catch (Exception e) {
                System.out.println("No se ha podido obtener los mejores parámetros");
            }
        } else {
            System.out.println("El programa necesita 1 argumento y ha recibido " + args.length);
        }
    }

    /**
     * Modifica la ruta del archivo .arff del que se cargan las instancias.
     *
     * @param pO La ruta del archivo que contiene todas las instancias.
     * @author Ander Cejudo
     */
    public void setPathO(String pO) {
        this.pathO = pO;
    }

    /**
     * Esquema de evaluación Hold-out en el cual se divide el conjunto {@link #data} en dos subgrupos: conjunto de entrenamiento con el 70% de las instancias y
     * conjunto de test con el 30% restante de las instancias. Además se utiliza un valor completamente aleatorio para randomizar {@link #data}. Guarda ambos conjuntos
     * en archivos diferentes y aplica {@link TransformRaw} sobre el conjunto de entrenamiento, obteniendo así el diccionario y el
     * train_bow y aplica {@link MakeCompatible} sobre el conjunto dev para adaptar el espacio de atributos del conjunto de dev
     * al conjunto de test. Más tarde carga ambos conjuntos procesados en los atributos {@link #train} y {@link #test} y realizá una selección
     * de atributos.
     *
     * @author Ander Cejudo
     */
    public void holdOut() {
        //RANDOMIZAMOS EL CONJUNTO COMPLETO
        double valRandom = Math.random() * 2001;
        data.randomize(new Random((long) valRandom));
        //70% TRAIN, 30% DEV (TEST SUPERVISADO)
        train = Utilidades.removePercentage(data, 70.0, true);
        test = Utilidades.removePercentage(data, 70.0, false);
        if (Utilidades.tieneAtributoString(data)) {
            try {
                //GUARDAMOS AMBOS CONJUTNOS PARA QUE LO PUEDA USAR TRANSFORMRAW y MAKECOMPATIBLE
                Utilidades.guardarArffRelativo(train, "train_raw.arff");
                Utilidades.guardarArffAbsoluto(test, "dev_raw.arff");
                //CONFIGURAMOS LOS PARÁMETROS PARA TRANSFORM RAW
                String[] args = {"train_raw.arff", "train_" + REPRESENTACIÓN + ".arff", REPRESENTACIÓN, "nonsparse"};
                //AQUÍ OBTENDREMOS BOW DEL TRAIN
                TransformRaw.main(args);
                //CARGAMOS EL CONJUNTO DE ENTRENAMIENTO
                this.train = Utilidades.archivo2Instancias("train_" + REPRESENTACIÓN + ".arff");
                //LOS FILTROS CAMBIAN LA CLASE A LA POSICIÓN 0
                Utilidades.setIndiceClase(0, train);
                Utilidades.setIndiceClase(indiceClase, test);
            } catch (Exception e) {
                System.out.println("No se ha podido hacer hold-out");
            }
        } else {
            Utilidades.setIndiceClase(indiceClase, train);
            Utilidades.setIndiceClase(indiceClase, test);
        }
        seleccionarAtributos();
    }

    /**
     * Selecciona atributos de los conjuntos de instancias de la clase.
     *
     * @author Ander Cejudo
     */
    public void seleccionarAtributos() {
        //EL FSS Y MAKECOMPATIBLE MUEVE LA CLASE AL 0 Y PONE EL CLASS INDEX EN 0
        int cuantos = (int) (train.numAttributes() * Double.parseDouble(CUANTOS_ATRIBUTOS));
        //APLICAMOS ATTRIBUTE SELECTION AL TRAIN
        if (!CUANTOS_ATRIBUTOS.equals("-1")) {
            this.train = Utilidades.seleccionarAtributos(train, cuantos + "", "DEF");
        }
        //ADAPTAMOS LOS ATRIBUTOS DEL DEV (TEST) A LOS QUE HAN QUEDADO EN TRAIN
        String dict = Utilidades.diccionario(train);
        //setIndiceClase(indiceClase,test);
        boolean tf = REPRESENTACIÓN.equalsIgnoreCase("tf");
        //test = fss.seleccionarAtributos(test,"-1","DEF");
        test = Utilidades.filtroDiccionario(test, dict, tf);
        if (Utilidades.tieneAtributoString(data)) {
            Utilidades.setIndiceClase(0, test);
        } else {
            Utilidades.setIndiceClase(indiceClase, test);
        }

    }

    /**
     * Obtiene e imprime los parámetros óptimos para el clasificado SVM relativo al conjunto {@link #data}. Los parámetros que se van a tener en cuenta son: kernel, gamma, c y degree.
     * <ul>
     *     <li>Kernel: define la función que va a crear el hiperplano: </li>
     *         <li>&nbsp;&nbsp;&nbsp;Linear: una recta (u'*v).</li>
     *         <li>&nbsp;&nbsp;&nbsp;Polynomial: un polinomio (gamma*u'*v + coef0)^degree.</li>
     *         <li>&nbsp;&nbsp;&nbsp;RBS: función de base radial (exp(-gamma*|u-v|^2)).</li>
     *         <li>&nbsp;&nbsp;&nbsp;Sigmoid: función que describe una evolución con un climax (tanh(gamma*u'*v + coef0)).</li>
     *      <li>Gamma: parámetro para funciones no lineales. En este caso será el que se estudie cuando el kernel sea RBF o Sigmoid. Cuanto mayor sea más intenta ajustar
     *      los datos dentro del campo. Si la gamma es alta solo se considerarán los puntos cercanos al hiperplano y si es baja, se tendrán en cuenta puntos lejanos.</li>
     *      <li>C (Coste): también denominado parámetro de castigo, indica cuanto te importa que haya instancias mal clasificadas. Cuanto más alto, mayor importancia
     *      se le da a que estén bien clasificadas. Se estudiará para la función lineal.</li>
     *      <li>Degree: Es el exponente de la función polinómica. </li>
     * </ul>
     *
     * @return Un par cuyo primer valor corresponde al indice del kernel y el segundo al valor del segundo parametro.
     * @throws Exception                      Si la evaluación de alguna configuración falla y no es posible encontrar los parámetros óptimos.
     * @throws ArrayIndexOutOfBoundsException Si el kernel vale -1 porque el conjunto es muy pequeño.
     * @author Ander Cejudo
     * @see <a href="https://weka.sourceforge.io/doc.stable/weka/classifiers/functions/LibSVM.html">LibSVM</a>
     * @see <a href="https://medium.com/all-things-ai/in-depth-parameter-tuning-for-svc-758215394769">Parameter tuning</a>
     */
    public AbstractMap.SimpleEntry<Integer, Double> mejoresParametros() throws Exception, ArrayIndexOutOfBoundsException {
        int indiceMin = Utilidades.indiceClaseMinoritaria(data), kernelOpt = -1, idxParameter;
        boolean modificado, mejorKernel;
        double valorOpt = 0.0;
        //PARA CADA TIPO DE KERNEL
        for (int i = 0; i < nombreKernels.length; i++) {
            //COMO RBF Y SIGMOID TIENEN EL MISMO PARÁMETRO A LOS 2 SE LES ASIGNA EL INDICE 2 PARA LA LISTA DE PARÁMETROS
            idxParameter = selIdx(i);
            //NOS PERMITE SABER SI TENEMOS QUE GUARDAR ESTE KERNEL CUANDO SE EVALÚA CON DIFERENTE V0ALORES DEL PARÁMETRO
            mejorKernel = false;
            for (int j = 0; j < valoresParams.get(idxParameter).length; j++) {
                double valParametro = valoresParams.get(idxParameter)[j];
                //SI LA CONFIGURACIÓN DE ESTOS 2 PARÁMETROS OBTIENE UN F-MEASURE MÁS ALTO
                modificado = fMeasureEval(indiceMin, i, valParametro);
                //ESTÉ KERNEL OBTENDRÁ MEJOR RESULTADO SI EN ALGUNA ITERACIÓN SE HA CONSEGUIDO MEJOR EL F-MEASURE ÓPTIMO
                mejorKernel = mejorKernel || modificado;
                //SE ACTUALIZA LE VALOR DEL SEGUNDO PARÁMETRO
                valorOpt = (modificado) ? valParametro : valorOpt;
            }
            //SI ESTE KERNEL HA CONSEGUIDO MEJORAR EL F-MEASURE
            if (mejorKernel) {
                kernelOpt = i;
            }
        }
        System.out.println("Kernel óptimo: " + nombreKernels[kernelOpt]);
        System.out.println(nombreParams[selIdx(kernelOpt)] + " optimo: " + valorOpt);
        return new AbstractMap.SimpleEntry<>(kernelOpt, valorOpt);
    }

    /**
     * En función del kernel se asigna el valor dado al parámetro correspondiente del clasificador dado.
     *
     * @param kernel       Indice del kernel que se utiliza.
     * @param valParametro Valor del segundo parámetro que se va a asignar al clasificado.
     * @param classifier   El clasificador que se está configurando.
     * @author Ander Cejudo
     */
    private void ajustarParametro(int kernel, double valParametro, LibSVM classifier) {
        if (kernel == 0) {
            classifier.setCost(valParametro);
        } else if (kernel == 1) {
            classifier.setDegree((int) valParametro);
        } else {
            classifier.setGamma(valParametro);
        }
    }

    /**
     * Sirve para ajustar el índice correspondiente a los tags. De esta manera, a los dos últimos tags se les asigna el mismo
     * índice.
     *
     * @param i Indice correspondiente a tags.
     * @return El valor del índice ajustado.
     * @author Ander Cejudo
     */
    public int selIdx(int i) {
        int idxParameter;
        if (i == LibSVM.TAGS_KERNELTYPE.length - 1) {
            idxParameter = i - 1;
        } else {
            idxParameter = i;
        }
        return idxParameter;
    }

    /**
     * Entrena el clasificador dado con el conjunto de entrenamiento y lo evalúa con el de test. Después calcula el f-measure de la clase
     * minoritaria y lo compara con el fmeasure global, cuyo valor corresponde con el fmeasure más alto que se ha obtenido.
     * , el cual se actualiza caso de que el fmeasure global sea menor que el que se acaba de calcular.
     *
     * @param iMin   El indice de la clase minoritaria. Lo recibe como parámetro dado que calcularlo en cada llamada al método tendría un cose muy elevado.
     * @param kernel El indice del kernel a utilizar.
     * @param val    El valor del segundo parámetro.
     * @return True si el valor de {@link #fOpt} ha sido actualizado y false en caso contrario.
     * @throws Exception Si por alguna razón no se ha podido entrenar o evaluar el modelo.
     * @author Ander Cejudo
     */
    public boolean fMeasureEval(double iMin, int kernel, double val) throws Exception {
        //REPETIMOS HOLD-OUT TANTAS VECES COMO SE HAYA ESPECIFICADO
        double[] fmeasures = new double[NUM_ITERACIONES];
        for (int i = 0; i < NUM_ITERACIONES; i++) {
            //SEPARAMOS EL CONJUNTO EN TRAIN Y TEST (DEV)
            holdOut();
            //CREAR Y CONFIGURA EL CLASIFICADOR
            LibSVM c = new LibSVM();
            c.setKernelType(new SelectedTag(kernel, LibSVM.TAGS_KERNELTYPE));
            ajustarParametro(kernel, val, c);
            c.buildClassifier(train);
            //EVALUAR EL CLASIFICADOR
            Evaluation eval = new Evaluation(test);
            eval.evaluateModel(c, test);
            fmeasures[i] = eval.fMeasure((int) iMin);
            imprimirProgreso();
        }
        //USAREMOS LA MEDIA DE TODOS LOS F-MEASURES OBTENIDOS
        double fmeasure = Utilidades.media(fmeasures);
        //SE GUARDA EL MAYOR
        boolean mejorFmes = fmeasure > fOpt;
        if (mejorFmes) {
            fOpt = fmeasure;
        }
        return mejorFmes;
    }

    /**
     * Imprime por pantalla el progreso de la optimización de parámetros.
     *
     * @author Ander Cejudo
     */
    public void imprimirProgreso() {
        DecimalFormat df2 = new DecimalFormat("###.##"); //INDICADOR DEL PROGRESO
        System.out.print("                                                       \r");
        if (cont < NUMERO_OPERACIONES) {
            System.out.print(df2.format(((double) cont / NUMERO_OPERACIONES) * 100) + "% completado\r");
            cont++;
        } else {
            //A VECES NO SE PUEDE CONTROLAR QUE SE SUPERE EL 100%, POR ESO LA CONDICIÓN
            System.out.print("100%\r");
        }
    }

    /**
     * Se obtiene la representación utilizada para los Strings.
     *
     * @return El tipo de representación.
     * @author Ander Cejudo
     */
    public String getRepresentacion() {
        return REPRESENTACIÓN;
    }

    /**
     * Crea el modelo predictor a partir de los 2 parámetros que maximizan su eficacia.
     *
     * @param kernel Indice del kernel a usar.
     * @param valor  Valor del parámetro asociado al kernel a usar.
     * @return Devuelve el clasificador entrenado con todo el conjunto de instancias.
     * @throws Exception Si no se ha podido entrenar el clasificador.
     * @author Ander Cejudo
     */
    public Classifier crearModeloPredictor(Integer kernel, Double valor) throws Exception {
        if (kernel == -1) {
            throw new UnsupportedOperationException();
        }
        LibSVM classifier = new LibSVM();
        classifier.setKernelType(new SelectedTag(kernel, LibSVM.TAGS_KERNELTYPE));
        int idxParametro = selIdx(kernel);
        ajustarParametro(idxParametro, valor, classifier);
        classifier.buildClassifier(data);
        return classifier;
    }

    /**
     * Estima la calidad del modelo utilizando el esquema de evaluación k-fold cross validation y la clase minoritaria.
     *
     * @param classifier Modelo cuya calidad se va a estimar.
     * @return Devuelve el texto con toda la información relativa a la calidad del modelo, el archivo utilizado y el valor de la clase minoritaria.
     * @throws Exception Si no se ha podido evaluar el modelo.
     * @author Ander Cejudo
     */
    public String calidadEstimada(Classifier classifier) throws Exception {
        int indiceMinoritario = Utilidades.indiceClaseMinoritaria(data);
        //K-FOLD CROSS VALIDATION (COTA REALISTA)
        int numRepeticiones = 10;
        Evaluation evaluator;
        double[] fmeasures = new double[numRepeticiones], recalls = new double[numRepeticiones], precisions = new double[numRepeticiones];
        for (int i = 0; i < numRepeticiones; i++) {
            evaluator = new Evaluation(data);
            double valRandom = Math.random() * 2001;
            //3-FOLD CROSS VALIDATION
            evaluator.crossValidateModel(classifier, data, 3, new Random((long) valRandom));
            fmeasures[i] = evaluator.weightedFMeasure();
            recalls[i] = evaluator.weightedRecall();
            precisions[i] = evaluator.weightedPrecision();
        }
        //NO HONESTA (COTA SUPERIOR)
        evaluator = new Evaluation(data);
        evaluator.evaluateModel(classifier, data);
        double fmeasureS = evaluator.weightedFMeasure();
        double reacallS = evaluator.weightedRecall();
        double precisionS = evaluator.weightedPrecision();
        DecimalFormat df = new DecimalFormat("#.####");
        String res = "------------CALIDAD ESTIMADA SVM------------";
        res += "\nArchivo: " + pathO;
        res += "\nClase minoritaria: " + data.classAttribute().value(indiceMinoritario);
        res += "\nCota realista: ";
        res += "\n    - Error: " + df.format(Utilidades.desviacionTipica(fmeasures));
        res += "\n    - F-measure: " + df.format(Utilidades.media(fmeasures));
        res += "\n    - Recall: " + df.format(Utilidades.media(recalls));
        res += "\n    - Precision: " + df.format(Utilidades.media(precisions));
        res += "\nCota superior: ";
        res += "\n    - F-measure: " + df.format(fmeasureS);
        res += "\n    - Recall: " + df.format(reacallS);
        res += "\n    - Precision: " + df.format(precisionS);
        return res;
    }


    /**
     * Elimina todos los archivos generados durante la ejecución del programa los cuales son necesarios para el funcionamiento del mismo.
     *
     * @author Ander Cejudo
     */
    public void eliminarArchivosGenerados() {
        Utilidades.eliminarArchivo("dev_raw.arff");
        Utilidades.eliminarArchivo("train_raw.arff");
        Utilidades.eliminarArchivo("train_" + REPRESENTACIÓN + ".arff");
        Utilidades.eliminarArchivo("dev_" + REPRESENTACIÓN + ".arff");
        Utilidades.eliminarArchivo("diccionario.txt");
    }
}
