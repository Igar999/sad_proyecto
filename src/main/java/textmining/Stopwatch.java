package textmining;

/**
 * Cronómetro usado para calcular el tiempo de ejecución de los programas.
 */
public class Stopwatch {

    /**
     * Tiempo en el que se instancia la clase.
     */
    private final long start;

    public Stopwatch() {
        start = System.currentTimeMillis();
    }

    /**
     * Tiempo que ha pasado desde que se ha instanciado la clase.
     *
     * @return El tiempo que ha pasado.
     */
    public double elapsedTime() {
        long now = System.currentTimeMillis();
        return (now - start) / 1000.0;
    }
}
