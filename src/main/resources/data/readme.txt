##################################################
Data source: https://trec.nist.gov
The TREC dataset is dataset for question classification consisting of open-domain, fact-based questions divided into broad semantic categories. It has both a six-class (TREC-6) and a fifty-class (TREC-50) version. Both have 5,452 training examples and 500 test examples 


##################################################
Related work: 
Papers with code: https://paperswithcode.com/sota/text-classification-on-trec-6

Supervised Learning of Universal Sentence Representations from Natural Language Inference DataoAlexis Conneau
Alexis Conneau, Douwe Kiela, Holger Schwenk
2018


##################################################
Alternative data-sets: 

Yahoo answers (Topic classification)

AG News
http://groups.di.unipi.it/~gulli/AG_corpus_of_news_articles.html
AG’s news corpus. It contains 496,835 categorized news articles from more than 2000 news sources. We choose the 4 largest classes from this corpus to construct our dataset, using only the title and description fields. The number of training samples for each class is 30,000 and testing 1900.

DBpedia
The DBpedia ontology dataset contains 560,000 training samples and 70,000 testing samples for each of 14 nonoverlapping classes from DBpedia. 


ACL-ARC dataset
Citation Function Corpus --Distribution Data
Paper: https://www.aclweb.org/anthology/Q18-1028.pdf
Data: https://www.cl.cam.ac.uk/~sht25/CFC.html
##################################################
