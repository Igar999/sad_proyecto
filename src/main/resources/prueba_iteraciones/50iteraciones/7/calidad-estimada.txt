------------CALIDAD ESTIMADA SVM------------
Archivo: balance-scale.arff
Clase minoritaria: B
Cota realista: 
    - Error: 0,0168
    - F-measure: 0,9788
    - Recall: 0,9837
    - Precision: 0,9747
Cota superior: 
    - F-measure: 1
    - Recall: 1
    - Precision: 1
Parámetros utilizados: 
    - Kernel: Función polinómica
    - Exponente (degree): 3.0
Tiempo de ejecución: 18.999s