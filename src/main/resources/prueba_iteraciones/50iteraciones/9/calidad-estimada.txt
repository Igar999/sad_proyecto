------------CALIDAD ESTIMADA SVM------------
Archivo: balance-scale.arff
Clase minoritaria: B
Cota realista: 
    - Error: 0,0223
    - F-measure: 0,9751
    - Recall: 0,9857
    - Precision: 0,9656
Cota superior: 
    - F-measure: 1
    - Recall: 1
    - Precision: 1
Parámetros utilizados: 
    - Kernel: Función polinómica
    - Exponente (degree): 3.0
Tiempo de ejecución: 21.336s