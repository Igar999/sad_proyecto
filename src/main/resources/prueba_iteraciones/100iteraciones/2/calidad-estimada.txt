------------CALIDAD ESTIMADA SVM------------
Archivo: balance-scale.arff
Clase minoritaria: B
Cota realista: 
    - Error: 0,0151
    - F-measure: 0,9774
    - Recall: 0,9735
    - Precision: 0,9816
Cota superior: 
    - F-measure: 1
    - Recall: 1
    - Precision: 1
Parámetros utilizados: 
    - Kernel: Función polinómica
    - Exponente (degree): 3.0
Tiempo de ejecución: 37.199s