------------CALIDAD ESTIMADA SVM------------
Archivo: balance-scale.arff
Clase minoritaria: B
Cota realista: 
    - Error: 0,0169
    - F-measure: 0,9842
    - Recall: 0,998
    - Precision: 0,9712
Cota superior: 
    - F-measure: 1
    - Recall: 1
    - Precision: 1
Parámetros utilizados: 
    - Kernel: Función polinómica
    - Exponente (degree): 3.0
Tiempo de ejecución: 178.44s